# kit

A CLI toolkit for the CI/CD environment portable to container image or bare metal.

![v1](https://gitlab.com/sailr/skunkworks/kit/uploads/1979aa25a5dcec63b7d0f5ee4cd6822c/image.png)

## Overview

Kit uses [github-flow](https://guides.github.com/introduction/flow/) to handle branch management.

### build

Kit builds out docker containers and Cloud Native Buildpacks.

### test

Testing is declared and managed by the application developers. Kit makes no determination about who, how, or what should test the application as such.

### deploy

Prerelease, branch-management, version-management, CHANGELOG-management, and post-backs (jira, issue, 3rd parties)

## Installing

Locally:

1. Download the binary from release
2. `chmod +x kit`
3. `mv kit /usr/bin/kit`

Container Image:

1. `kit deploy`

## :open_hands: Contributing to Kit

Please check out the [Contributing Guidelines](./CONTRIBUTING.md).

To build the binary locally:

```bash
# Create images to test locally
make build
```

docker:

```bash
# Start a container and start a shell session
docker run -it sailr/kit:latest bash -l
```

To add a function:

`cobra add <command_name>`

cobra will add a `cmd/` file for implementation that is referenced in `kit.go` for usage.


## License & Maintainers

### Maintained by

Sailr.Co Crew

### License

[Apache 2.0](./LICENSE)
