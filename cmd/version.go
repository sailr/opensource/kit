package cmd

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/spf13/cobra"
)

// checkErr panics on error
func checkErr(e error) {
	if e != nil {
		panic(e)
	}
}

// CreateVersionFile creates a VERSION file if one doesn't exist
func CreateVersionFile() {
	defaultVersion := []byte("v0.0.1")
	f, err := os.Create("./VERSION")
	checkErr(err)
	defer f.Close()
	f.Write(defaultVersion)
}

// ReadVersion returns the version from VERSION as a string
func ReadVersion() string {
	fileRead, err := ioutil.ReadFile("./VERSION")
	checkErr(err)
	fmt.Print(string(fileRead))
	return string(fileRead)
}

// GetOrCreateVersionFile gets the current VERSION file or creates one if it doesn't exist
func GetOrCreateVersionFile() string {
	if _, err := os.Stat("./VERSION"); os.IsNotExist(err) {
		CreateVersionFile()
		return ReadVersion()
	}
		return ReadVersion()
}

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("version called")
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
