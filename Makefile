build:
	go build -o bin

run: clean build
	bin/kit

clean:
	rm -rf bin/kit

docker-build:
	docker build -t registry.gitlab.com/sailr/skunkworks/kit .

docker-push:
	docker push registry.gitlab.com/sailr/skunkworks/kit
