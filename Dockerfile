FROM golang:alpine AS build-stage
WORKDIR /kit
RUN apk add --update git
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o kit /kit

FROM scratch
COPY --from=build-stage /kit/kit .
CMD ["./kit"]
